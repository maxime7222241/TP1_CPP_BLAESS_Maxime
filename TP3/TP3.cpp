#include <iostream>
#include <vector>

// En binome avec Toussaint Paul

class Plateau {
protected:
    std::vector<std::vector<char>> tableau;

public:
    Plateau(int lignes, int colonnes) : tableau(lignes, std::vector<char>(colonnes, ' ')) {}

    void afficher() const {
        std::cout << "-------------" << std::endl;
        for (const auto& ligne : tableau) {
            std::cout << "| ";
            for (char caseJeu : ligne) {
                std::cout << caseJeu << " | ";
            }
            std::cout << std::endl << "-------------" << std::endl;
        }
    }

    bool caseVide(int ligne, int colonne) const {
        return ligne >= 0 && ligne < tableau.size() && colonne >= 0 && colonne < tableau[0].size() && tableau[ligne][colonne] == ' ';
    }

    void placerPion(int ligne, int colonne, char joueur) {
        tableau[ligne][colonne] = joueur;
    }

    virtual bool estMatchNul() const = 0;
    virtual bool estPartieTerminee() const = 0;
};

class Morpion : public Plateau {
public:
    Morpion() : Plateau(3, 3) {}

    bool estMatchNul() const override {
        for (const auto& ligne : tableau) {
            for (char caseJeu : ligne) {
                if (caseJeu == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    bool estPartieTerminee() const override {
        for (int i = 0; i < 3; ++i) {
            if (tableau[i][0] == tableau[i][1] && tableau[i][1] == tableau[i][2] && tableau[i][0] != ' ') {
                return true; // lignes
            }
            if (tableau[0][i] == tableau[1][i] && tableau[1][i] == tableau[2][i] && tableau[0][i] != ' ') {
                return true; // colonnes
            }
        }
        // diagonales
        return (tableau[0][0] == tableau[1][1] && tableau[1][1] == tableau[2][2] && tableau[0][0] != ' ') ||
               (tableau[0][2] == tableau[1][1] && tableau[1][1] == tableau[2][0] && tableau[0][2] != ' ');
    }
};

class Puissance4 : public Plateau {
public:
    Puissance4() : Plateau(4, 7) {}

    bool estMatchNul() const override {
        for (const auto& ligne : tableau) {
            for (char caseJeu : ligne) {
                if (caseJeu == ' ') {
                    return false;
                }
            }
        }
        return true;
    }

    bool estPartieTerminee() const override {
        // logique de vérification de victoire pour Puissance 4
        // (à compléter selon les règles du jeu)
        return false;
    }
};

class Joueur {
private:
    char symbole;

public:
    Joueur(char symbole) : symbole(symbole) {}

    char getSymbole() const {
        return symbole;
    }
};

class InterfaceUtilisateur {
public:
    static int obtenirChoixJeu() {
        int choix;
        std::cout << "Choisissez le jeu :\n1. Morpion\n2. Puissance 4\nEntrez votre choix : ";
        std::cin >> choix;
        return choix;
    }

    static void afficherMessageVictoire(char joueur) {
        std::cout << "Joueur " << joueur << " gagne !" << std::endl;
    }

    static void afficherMessageMatchNul() {
        std::cout << "Match nul !" << std::endl;
    }

    static std::pair<int, int> obtenirMouvement() {
        int ligne, colonne;
        std::cout << "Entrez la ligne (1-3) et la colonne (1-3): ";
        std::cin >> ligne >> colonne;
        return std::make_pair(ligne - 1, colonne - 1);
    }
};

class Jeu {
public:
    static void jouer() {
        int choix = InterfaceUtilisateur::obtenirChoixJeu();

        if (choix == 1) {
            jouerPartie<Morpion>();
        } else if (choix == 2) {
            jouerPartie<Puissance4>();
        } else {
            std::cout << "Choix invalide. Veuillez choisir 1 pour Morpion ou 2 pour Puissance 4." << std::endl;
        }
    }

private:
    template <class T>
    static void jouerPartie() {
        T jeu;
        Joueur joueur1('X');
        Joueur joueur2('O');
        Joueur joueurActuel = joueur1;

        int ligne, colonne;
        do {
            jeu.afficher();
            std::pair<int, int> mouvement = InterfaceUtilisateur::obtenirMouvement();
            ligne = mouvement.first;
            colonne = mouvement.second;

            if (jeu.caseVide(ligne, colonne)) {
                jeu.placerPion(ligne, colonne, joueurActuel.getSymbole());

                if (jeu.estPartieTerminee()) {
                    jeu.afficher();
                    InterfaceUtilisateur::afficherMessageVictoire(joueurActuel.getSymbole());
                    return;
                }

                if (jeu.estMatchNul()) {
                    jeu.afficher();
                    InterfaceUtilisateur::afficherMessageMatchNul();
                    return;
                }

                joueurActuel = (joueurActuel.getSymbole() == joueur1.getSymbole()) ? joueur2 : joueur1;
            } else {
                std::cout << "Mouvement invalide. Veuillez réessayer." << std::endl;
            }
        } while (true);
    }
};

int main() {
    Jeu::jouer();

    return 0;
}
