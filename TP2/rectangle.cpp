#include <iostream>

struct Point {
    float x;
    float y;
};

class Rectangle {
private:
    int longueur;
    int largeur;
    Point coinSuperieurGauche;

public:
    Rectangle(int length, int width, float x, float y) {
        longueur = length;
        largeur = width;
        coinSuperieurGauche.x = x;
        coinSuperieurGauche.y = y;
    }

    int getLongueur() const {
        return longueur;
    }

    void setLongueur(int length) {
        longueur = length;
    }

    int getLargeur() const {
        return largeur;
    }

    void setLargeur(int width) {
        largeur = width;
    }

    Point getCoinSuperieurGauche() const {
        return coinSuperieurGauche;
    }

    void setCoinSuperieurGauche(float x, float y) {
        coinSuperieurGauche.x = x;
        coinSuperieurGauche.y = y;
    }

    int calculerPerimetre() const {
        return 2 * (longueur + largeur);
    }

    int calculerSurface() const {
        return longueur * largeur;
    }
};

int main() {
    Rectangle myRectangle(5, 3, 2.0, 4.0);

    std::cout << "Longueur : " << myRectangle.getLongueur() << std::endl;
    std::cout << "Largeur : " << myRectangle.getLargeur() << std::endl;

    Point coin = myRectangle.getCoinSuperieurGauche();
    std::cout << "Coin supérieur gauche : (" << coin.x << ", " << coin.y << ")" << std::endl;

    std::cout << "Périmètre : " << myRectangle.calculerPerimetre() << std::endl;
    std::cout << "Surface : " << myRectangle.calculerSurface() << std::endl;

    return 0;
}
