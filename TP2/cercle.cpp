#define _USE_MATH_DEFINES
#include <iostream>
#include <cmath>

struct Point {
    float x;
    float y;
};

class Cercle {
private:
    Point centre;
    int diametre;

public:
    Cercle(float x, float y, int diameter) {
        centre.x = x;
        centre.y = y;
        diametre = diameter;
    }

    Point getCentre() const {
        return centre;
    }

    void setCentre(float x, float y) {
        centre.x = x;
        centre.y = y;
    }

    int getDiametre() const {
        return diametre;
    }

    void setDiametre(int diameter) {
        diametre = diameter;
    }

    float calculerPerimetre() const {
        return M_PI * diametre;
    }

    float calculerSurface() const {
        return 0.25 * M_PI * diametre * diametre;
    }

    bool estSurCercle(float x, float y) const {
        float distanceAuCentre = std::sqrt((x - centre.x) * (x - centre.x) + (y - centre.y) * (y - centre.y));
        return std::abs(distanceAuCentre - (diametre / 2.0)) < 1e-6;
    }

    bool estAInterieurCercle(float x, float y) const {
        float distanceAuCentre = std::sqrt((x - centre.x) * (x - centre.x) + (y - centre.y) * (y - centre.y));
        return distanceAuCentre < (diametre / 2.0);
    }
};

int main() {
    Cercle myCercle(2.0, 3.0, 5);

    std::cout << "Centre : (" << myCercle.getCentre().x << ", " << myCercle.getCentre().y << ")" << std::endl;
    std::cout << "Diamètre : " << myCercle.getDiametre() << std::endl;
    std::cout << "Périmètre : " << myCercle.calculerPerimetre() << std::endl;
    std::cout << "Surface : " << myCercle.calculerSurface() << std::endl;

    float pointX = 4.0;
    float pointY = 3.0;
    if (myCercle.estSurCercle(pointX, pointY)) {
        std::cout << "Le point (" << pointX << ", " << pointY << ") est sur le cercle." << std::endl;
    } else {
        std::cout << "Le point (" << pointX << ", " << pointY << ") n'est pas sur le cercle." << std::endl;
    }

    float pointX2 = 2.5;
    float pointY2 = 3.5;
    if (myCercle.estAInterieurCercle(pointX2, pointY2)) {
        std::cout << "Le point (" << pointX2 << ", " << pointY2 << ") est à l'intérieur du cercle." << std::endl;
    } else {
        std::cout << "Le point (" << pointX2 << ", " << pointY2 << ") n'est pas à l'intérieur du cercle." << std::endl;
    }

    return 0;
}
