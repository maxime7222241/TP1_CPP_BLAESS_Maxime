
#include <iostream>
#include <cmath>

struct Point {
    float x;
    float y;
};

class Triangle {
private:
    Point sommetA;
    Point sommetB;
    Point sommetC;

public:
    Triangle(float x1, float y1, float x2, float y2, float x3, float y3) {
        sommetA.x = x1;
        sommetA.y = y1;
        sommetB.x = x2;
        sommetB.y = y2;
        sommetC.x = x3;
        sommetC.y = y3;
    }

    Point getSommetA() const {
        return sommetA;
    }

    void setSommetA(float x, float y) {
        sommetA.x = x;
        sommetA.y = y;
    }

    Point getSommetB() const {
        return sommetB;
    }

    void setSommetB(float x, float y) {
        sommetB.x = x;
        sommetB.y = y;
    }

    Point getSommetC() const {
        return sommetC;
    }

    void setSommetC(float x, float y) {
        sommetC.x = x;
        sommetC.y = y;
    }

    float calculerDistance(Point p1, Point p2) const {
        return std::sqrt(std::pow(p2.x - p1.x, 2) + std::pow(p2.y - p1.y, 2));
    }

    float calculerBase() const {
        float a = calculerDistance(sommetA, sommetB);
        float b = calculerDistance(sommetB, sommetC);
        float c = calculerDistance(sommetC, sommetA);

        return std::max(std::max(a, b), c);
    }

    float calculerHauteur() const {
        float surface = calculerSurface();
        float base = calculerBase();
        return 2.0 * surface / base;
    }

    float calculerSurface() const {
        float a = calculerDistance(sommetA, sommetB);
        float b = calculerDistance(sommetB, sommetC);
        float c = calculerDistance(sommetC, sommetA);
        float s = (a + b + c) / 2.0;

        return std::sqrt(s * (s - a) * (s - b) * (s - c));
    }

    void obtenirLongueurs(float &a, float &b, float &c) const {
        a = calculerDistance(sommetA, sommetB);
        b = calculerDistance(sommetB, sommetC);
        c = calculerDistance(sommetC, sommetA);
    }

    bool estIsocele() const {
        float a, b, c;
        obtenirLongueurs(a, b, c);
        return (a == b) || (b == c) || (a == c);
    }

    bool estRectangle() const {
        float a, b, c;
        obtenirLongueurs(a, b, c);
        return (std::abs(a * a + b * b - c * c) < 1e-6) ||
               (std::abs(b * b + c * c - a * a) < 1e-6) ||
               (std::abs(c * c + a * a - b * b) < 1e-6);
    }

    bool estEquilateral() const {
        float a, b, c;
        obtenirLongueurs(a, b, c);
        return (a == b) && (b == c);
    }
};

int main() {
    Triangle myTriangle(0.0, 0.0, 3.0, 0.0, 1.5, 2.6);

    std::cout << "Base : " << myTriangle.calculerBase() << std::endl;
    std::cout << "Hauteur : " << myTriangle.calculerHauteur() << std::endl;
    std::cout << "Surface : " << myTriangle.calculerSurface() << std::endl;

    if (myTriangle.estIsocele()) {
        std::cout << "Le triangle est isocele." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas isocele." << std::endl;
    }

    if (myTriangle.estRectangle()) {
        std::cout << "Le triangle est rectangle." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas rectangle." << std::endl;
    }

    if (myTriangle.estEquilateral()) {
        std::cout << "Le triangle est equilateral." << std::endl;
    } else {
        std::cout << "Le triangle n'est pas equilateral." << std::endl;
    }

    return 0;
}
