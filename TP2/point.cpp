#include <iostream>

struct Point {
    float x;
    float y;
};

int main() {
    Point myPoint;

    myPoint.x = 2.5;
    myPoint.y = 3.7;

    std::cout << "Coordonnées du point : (" << myPoint.x << ", " << myPoint.y << ")" << std::endl;

    return 0;
}
