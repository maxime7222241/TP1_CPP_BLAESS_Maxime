#include <iostream>
#include <cmath>

struct Point {
    float x;
    float y;
};


class Carre {
private:
    int cote;
    Point coinSuperieurGauche; 

public:
    Carre(int sideLength, float x, float y) {
        cote = sideLength;
        coinSuperieurGauche.x = x;
        coinSuperieurGauche.y = y;
    }

    int getCote() const {
        return cote;
    }

    void setCote(int sideLength) {
        cote = sideLength;
    }

    Point getCoinSuperieurGauche() const {
        return coinSuperieurGauche;
    }

    void setCoinSuperieurGauche(float x, float y) {
        coinSuperieurGauche.x = x;
        coinSuperieurGauche.y = y;
    }

    int calculerBase() const {
        return cote;
    }

    int calculerHauteur() const {
        return cote;
    }

    int calculerSurface() const {
        return cote * cote;
    }

    int calculerPerimetre() const {
        return 4 * cote;
    }
};

int main() {
    Carre mySquare(5, 2.0, 3.0);

    std::cout << "Côté : " << mySquare.getCote() << std::endl;
    std::cout << "Base : " << mySquare.calculerBase() << std::endl;
    std::cout << "Hauteur : " << mySquare.calculerHauteur() << std::endl;
    std::cout << "Surface : " << mySquare.calculerSurface() << std::endl;
    std::cout << "Périmètre : " << mySquare.calculerPerimetre() << std::endl;

    return 0;
}
