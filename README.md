En binome avec Toussaint PAUL

Points d'amélioration :
Responsabilités multiples dans une seule classe :

Dans les classes Morpion et Puissance4, la classe gère à la fois les règles du jeu, le plateau et les interactions avec l'utilisateur. Cela peut rendre le code complexe et difficile à maintenir. Il est préférable de séparer ces responsabilités en différentes classes.
Redondance de code :

Il y a une certaine redondance dans les boucles de vérification de victoire pour le Morpion et le Puissance 4. Cette logique pourrait être rendue plus générique et réutilisable.
Code monolithique dans la fonction main :

La fonction main a une logique complexe qui pourrait être simplifiée en utilisant des classes plus petites et modulaires.
Absence de gestion des erreurs d'entrée utilisateur :

Le code ne gère pas les erreurs d'entrée utilisateur de manière robuste, ce qui peut conduire à des comportements imprévisibles si l'utilisateur entre des données incorrectes.
Refactoring Proposé :
Séparation des Responsabilités :

Créer une classe distincte pour représenter le plateau de jeu.
Créer une classe pour gérer les règles spécifiques à chaque jeu (Morpion, Puissance 4).
Utiliser une classe pour gérer les interactions avec l'utilisateur.
Utilisation de Fonctions Génériques :

Utiliser des fonctions génériques pour la vérification de victoire, ce qui évite la redondance de code.
Modularité et Lisible :

Séparer la logique de jeu en fonctions modulaires et compréhensibles.
Gestion des Erreurs :

Ajouter une gestion des erreurs d'entrée utilisateur plus robuste.
Utilisation de Classes Virtuelles :

Utiliser des classes virtuelles pour créer une structure plus générique, permettant d'ajouter d'autres jeux plus facilement.
Le refactoring ci-dessus vise à rendre le code plus lisible, modulaire, et extensible, en suivant les principes SOLID et en améliorant la gestion des responsabilités au sein du programme.

Toute les modifications on était fait en un seul commit